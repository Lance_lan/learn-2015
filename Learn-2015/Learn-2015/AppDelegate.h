//
//  AppDelegate.h
//  Learn-2015
//
//  Created by Lance on 15/6/23.
//  Copyright (c) 2015年 Lance. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

