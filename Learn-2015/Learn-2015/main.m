//
//  main.m
//  Learn-2015
//
//  Created by Lance on 15/6/23.
//  Copyright (c) 2015年 Lance. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
